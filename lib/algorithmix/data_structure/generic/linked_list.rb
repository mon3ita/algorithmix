# Documentation: https://github.com/monzita/algorithmix/wiki/LinkedList

module Algorithmix
  module DataStructure
    module Generic

      class LinkedList
        attr_reader :length, :size, :front

        # Creates a new linked list object.
        #
        # @param obj [#to_a] any object which responds to to_a method
        # @kwarg copy [true, false] If specified with true it will copy the given object. By default is set to false.
        # @raise ArgumentError if given object doesn't respond to to_a method
        # @return [LinkedList] a newly created linked list
        def initialize(obj = nil, copy: false)
          @length = @size = 0
          @front = nil

          obj.nil? ? nil : from_obj(obj, copy)
        end

        # Assigns a content of an object to content of the linked list, removing all previously inserted elements.
        #
        # @param obj [#to_a]
        # @kwarg copy [true, false] If specified with true it will copy the given object. By default is set to false.
        # @raise ArgumentError if given object doesn't respond to to_a method
        # @return [LinkedList] self object
        def assign(obj, copy: false)
          from_obj(obj, copy)
        end

        # Inserts a new element at the beginning of the list.
        #
        # @param value
        # @return newly inserted element
        def push_front(value)
          tail = @front
          rand_id = Random.new.rand((2**(0.size * 8 -2) -1) / 2)
          id = @front.nil? ? rand_id : @front.id
          @front = Node.new(value, tail, id)
          @length = @size += 1
          
          @front
        end

        # (see #push_front)
        def >>(value)
          push_front(value)
        end

        # Removes element at the beginning of the list.
        # 
        # @raise Algorithmix::EmptyContainerError if the list is empty.
        # @return removed value
        def pop_front
          raise EmptyContainerError, "The Linked list is empty." if @front.nil?

          value = @front.value
          @front = @front.next_node
          @length = @size -= 1
          value
        end

        # Inserts an element after the position specified by iterator.
        #
        # @param it, value [LinkedList::Iterator, #any]
        # @raise ArgumentError, if given iterator doesn't point to any element of the list.
        # @return self object
        def insert_after(it, value)
          raise ArgumentError, "Undefined method LinkedList#insert_after for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id

          tail = it.current.next_node
          it.current.send(:next_node=, Node.new(value, it.current, it.current.id))
          it.current.next_node.send(:next_node=, tail)
          @length = @size += 1
          
          it.next_node
        end

        # Removes the element after the position specified by the iterator.
        # 
        # @param it[LinkedList::Iterator]
        # @raise ArgumentIterator, if given iterator doesn't point to any element of the list.
        # @return removed element
        def erase_after(it)
          raise ArgumentError, "Undefined method LinkedList#erase_after for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id
          raise OutOfBound, "There is no more elements" if it.current.next_node.nil?

          node = it.current.next_node
          it.current.send(:next_node=, it.current.next_node.next_node)
          @size = @length -= 1
          node.value
        end

        # Returns true if there are no elements in the list.
        def empty?
          @front.nil?
        end

        # Compares contents of two linked lists.
        #
        # @param linked_list [LinkedList]
        # @raise ArgumentError if given object is not a linked_list
        # @return [true, false] true if both lists are equal, false otherwise
        def ==(linked_list)
          raise ArgumentError, "Undefined method LinkedList#== for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          return false if @length != linked_list.length

          it = @front
          other_it = linked_list.front
          until it.nil?
            return false if it.value != other_it.value
            it = it.next_node
          end

          true
        end

        # (see #==)
        def eql?(linked_list)
          raise ArgumentError, "Undefined method LinkedList#eql? for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self == linked_list
        end

        # Compares contents of two linked lists.
        #
        # @param linked_list [LinkedList]
        # @raise ArgumentError if given object is not a linked_list
        # @return [true, false] true if both lists are different, false otherwise
        def !=(linked_list)
          raise ArgumentError, "Undefined method LinkedList#!= for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          return true if @length != linked_list.length

          it = @front
          other_it = linked_list.front
          until it.nil?
            return true if it.value != other_it.value
            it = it.next_node
          end

          false
        end

        # (see #!=)
        def diff?(linked_list)
          raise ArgumentError, "Undefined method LinkedList#diff? for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self != linked_list
        end

        # Compares contents of two linked lists.
        #
        # @param linked_list [LinkedList]
        # @raise ArgumentError if given object is not a linked_list
        # @return 
        #       -1 if content of the self object is greater than content of the second.
        #       0 if contents are equal.
        #       1 if content of the self object is less than content of the first.
        def <=>(linked_list)
          raise ArgumentError, "Undefined method LinkedList#<=> for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          to_a <=> linked_list.to_a
        end

        # Converts content of the list to an array.
        def to_a
          convert
        end

        # Clears content of the list.
        def clear
          @front = nil
          @length = @size = 0
          self
        end

        # Concatenates contents of self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] a new linked list
        def +(linked_list)
          raise ArgumentError, "Undefined method LinkedList#+ for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          LinkedList.new(to_a.reverse + linked_list.to_a.reverse)
        end

        # (see #+)
        def concat(linked_list)
          raise ArgumentError, "Undefined method LinkedList#concat for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self + linked_list
        end

        # (see #+)
        def merge(linked_list)
          raise ArgumentError, "Undefined method LinkedList#merge for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self + linked_list
        end

        # Concatenates contents of self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] self object
        def concat!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#concat! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          from_obj(to_a.reverse + linked_list.to_a.reverse)
        end

        # (see #concat)
        def merge!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#merge! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          from_obj(to_a.reverse + linked_list.to_a.reverse)
        end

        # Finds the difference of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] a new linked list
        def -(linked_list)
          raise ArgumentError, "Undefined method LinkedList#- for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          LinkedList.new(to_a.reverse - linked_list.to_a.reverse)
        end

        # (see #-)
        def difference(linked_list)
          raise ArgumentError, "Undefined method LinkedList#difference for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self - linked_list
        end

        # Finds the difference of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] self object
        def difference!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#difference! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          from_obj(to_a.reverse - linked_list.to_a.reverse)
        end

        # Finds the intersection of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] a new linked list
        def &(linked_list)
          raise ArgumentError, "Undefined method LinkedList#& for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          LinkedList.new(to_a.reverse & linked_list.to_a.reverse)
        end

        # (see #&)
        def intersect(linked_list)
          raise ArgumentError, "Undefined method LinkedList#intersect for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self & linked_list
        end

        # Finds the intersection of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] self object
        def intersect!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#intersect! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          from_obj(to_a.reverse & linked_list.to_a.reverse)
        end

        # Finds the union of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] a new linked list
        def |(linked_list)
          raise ArgumentError, "Undefined method LinkedList#| for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          LinkedList.new(to_a.reverse | linked_list.to_a.reverse)
        end

        # (see #|)
        def union(linked_list)
          raise ArgumentError, "Undefined method LinkedList#union for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self | linked_list
        end

        # Finds the union of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] self object
        def union!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#union! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          from_obj(to_a.reverse | linked_list.to_a.reverse)
        end

        # Finds the symmetric_difference of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] a new linked list
        def ^(linked_list)
          raise ArgumentError, "Undefined method LinkedList#^ for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self_a, linked_list_a = to_a.reverse, linked_list.to_a.reverse
          LinkedList.new((self_a | linked_list_a) - (self_a & linked_list_a))
        end

        # (see #^)
        def symmetric_difference(linked_list)
          raise ArgumentError, "Undefined method LinkedList#symmetric_difference for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self ^ linked_list
        end

        # Finds the symmetric_difference of contents of the self object and linked_list given as argument.
        #
        # @param linked_list [LinedList]
        # @raise ArgumentError, if given object is not a linked list
        # @return [LinkedList] self object
        def symmetric_difference!(linked_list)
          raise ArgumentError, "Undefined method LinkedList#symmetric_difference! for #{linked_list}:#{linked_list.class}" unless linked_list.is_a?(LinkedList)
          self_a, linked_list_a = to_a.reverse, linked_list.to_a.reverse
          from_obj((self_a | linked_list_a) - (self_a & linked_list_a))
        end

        # Filters elements of the list by given condition.
        #
        # @param &block
        # @return [LinkedList] a new linked list
        def select(&block)
          LinkedList.new(to_a.reverse.select { |e| block.call(e) })
        end

        # Filters elements of the list by given condition.
        #
        # @param &block
        # @return [LinkedList] self object
        def select!(&block)
          from_obj(to_a.reverse.select { |e| block.call(e) })
        end

         # (see #select)
        def filter(&block)
          select(&block)
        end

        # (see #select!)
        def filter!(&block)
          select!(&block)
        end

        # (see #select)
        def find_all(&block)
          select(&block)
        end

        # (see #select!)
        def find_all!(&block)
          select!(&block)
        end

        # Applies a function to each element of the list.
        #
        # @param &block
        # @return [LinkedList] a new linked_list
        def map(&block)
          LinkedList.new(to_a.reverse.map { |e| block.call(e) })
        end

        # Applies a function to each element of the list.
        #
        # @param &block
        # @return [LinkedList] self object
        def map!(&block)
          from_obj(to_a.reverse.map { |e| block.call(e) })
        end

        # (see #map)
        def apply(&block)
          map(&block)
        end

        # (see #map!)
        def apply!(&block)
          map!(&block)
        end

        # Iterator class which gives couple of useful methods, for moving through the elements of a given list.
        class Iterator
          attr_reader :current, :next_node

          def initialize(node)
            raise ArgumentError, "Invalid argument" unless node.is_a?(Node) || node.is_a?(LinkedList)

            @begin = @node.is_a?(Node) ? node : node.front
            @current = @node.is_a?(Node) ? node : node.front
            @next_node = @node.is_a?(Node) ? node : node.front

            @next_node = @next_node.next_node
          end

          # Resets the current state of the iterator.
          # All iterators will point to the front of the list.
          def begin
            @current = @next_node = @begin
          end

          # Moves the iterator to the next position, and sets the current iterator to the previous position
          # of the next node.
          def next
            raise OutOfBound, "No more elements." if @next_node.nil?
            @current = @next_node
            @next_node = @next_node.next_node
            self
          end

          # Returns the value at the current node.
          def value
            @current.value
          end
        end

        private

        class Node
          attr_reader :id
          attr_accessor :value, :next_node

          def initialize(value = nil, next_node = nil, id = nil)
            @value = value
            @next_node = next_node
            @id = id
          end

          private :next_node=
        end

        def convert
          result = []
          it = @front
          until it.nil?
            result << it.value
            it = it.next_node
          end

          result
        end

        def from_obj(obj, copy=false)
          raise ArgumentError, "Undefined method #to_a for #{obj}:{obj.class}" unless obj.respond_to?(:to_a)

          @front = nil
          rand_id = Random.new.rand((2**(0.size * 8 -2) -1) / 2)
          obj.to_a.each do |e|
            tail = @front
            @front = Node.new(copy ? e.dup : e, tail, rand_id)
          end
          @length = @size = obj.to_a.length
          self
        end

      end
    end
  end
end