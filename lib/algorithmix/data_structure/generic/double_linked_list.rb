# Documentation: https://github.com/monzita/algorithmix/wiki/DoubleLinkedList

module Algorithmix
  module DataStructure
    module Generic

      class DoubleLinkedList

        attr_reader :length, :size, :front, :back

        # Creates a new doubly linked list.
        #
        # @param obj [#to_a] can be any object which responds to #to_a method
        # @kwarg copy [true, false] if it's set to true, will copy each element of the object to the content of the list
        # @raise ArgumentError, if object is provided, but it doesn't respond to #to_a method
        # @return [DoubleLinkedList] newly created doubly linked list 
        def initialize(obj = nil, copy: false)
          @front = @back = nil
          @length = @size = 0

          obj.nil? ? nil : from_obj(obj, copy)
        end

        # Copies content of an object to content of the list.
        #
        # @param obj [#to_a]
        # @kwarg copy [true, false]
        # @return [DobuleLinkedList] self object
        def assign(obj, copy: false)
          from_obj(obj, copy)
        end

        # Inserts an element at the beginning of the list.
        #
        # @param value
        # @return [Node] front element
        def push_front(value)
          rand_id = Random.new.rand((2**(0.size * 8 -2) -1) / 2)
          id = @front.nil? ? rand_id : @front.id

          if @front.nil?
            @front = @back = Node.new(value, nil, nil, id)
          else
            tail = @front
            @front = Node.new(value, nil, tail, id)
            tail.send(:prev_node=, @front)
          end

          @length = @size += 1

          self
        end

        # (see #push_front)
        def >>(value)
          push_front(value)
        end

        # Inserts an element at the end of the list.
        #
        # @param value
        # @return [Node] last element
        def push_back(value)
          rand_id = Random.new.rand((2**(0.size * 8 -2) -1) / 2)
          id = @front.nil? ? rand_id : @front.id

          if @front.nil?
            @front = @back = Node.new(value, nil, nil, id)
          else
            prev = @back
            @back = Node.new(value, prev, nil, id)
            prev.send(:next_node=, @back)
          end

          @length = @size += 1

          self
        end

        # (see #push_back)
        def <<(value)
          push_back(value)
        end

        # Removes front element of the list.
        #
        # @raise Algorithmix::EmptyContainerError, if there are no elements in the list.
        # @return value of the removed element
        def pop_front
          raise EmptyContainerError, "The list is empty." if @front.nil?

          value = @front.value
          @front = @front.next_node
          @front.send(:prev_node=, nil)

          @back = @front.nil? ? nil : @back

          @length = @size -= 1

          value
        end

        # Removes last element of the list.
        #
        # @raise Algorithmix::EmptyContainerError, if there are no elements in the list.
        # @return removed element
        def pop_back
          raise EmptyContainerError, "The list is empty." if @front.nil?

          value = @back.value
          @back = @back.prev_node
          @back.send(:next_node=, nil)

          @front = @back.nil? ? nil : @front

          @length = @size -= 1

          value
        end

        # Inserts an element before given position, specified by the iterator.
        #
        # @param it [Node]
        # @param value
        # @raise ArgumentError, if the given iterator is invalid, or is not from type Node.
        # @return [DoubleLinkedList] self object
        def insert_before(it, value)
          raise ArgumentError, "Undefined method DoubleLinkedList#insert_before for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id
          
          inserted = nil
          if it.current.__id__.eql?(@front.__id__)
            push_front(value)
            inserted = @front
          else
            prev = it.current.prev_node
            prev.send(:next_node=, Node.new(value, prev.prev_node, it.current, it.current.id))
            it.current.send(:prev_node=, prev.next_node)
            @length = @size += 1
            inserted = prev.next_node
          end

          inserted
        end

        # Inserts an element after given position, specified by the iterator.
        #
        # @param it [Node]
        # @param value
        # @raise ArgumentError, if the given iterator is invalid, or is not from type Node.
        # @return [DoubleLinkedList] self object
        def insert_after(it, value)
          raise ArgumentError, "Undefined method DoubleLinkedList#insert_after for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id

          inserted = nil
          if it.current.__id__.eql?(@back.__id__)
            push_back(value)
            inserted = @back
          else
            tail = it.current.next_node
            it.current.send(:next_node=, Node.new(value, it.current, tail, it.current.id))
            tail.send(:prev_node=, it.current.next_node)

            @length = @size += 1

            inserted = tail.prev_node
          end
          
          inserted
        end

        # Removes the element after given position, specified by the iterator.
        #
        # @param it [Node]
        # @raise ArgumentError, if the given iterator is invalid, or is not from type Node.
        # @raise EmptyContainerError, if the list is empty.
        # @raise OutOfBound, if there are no elements after the given iterator.
        # @return removed element
        def erase_after(it)
          raise ArgumentError, "Undefined method DoubleLinkedList#erase_after for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id
          raise EmptyContainerError, "The list is empty" if @front.nil?
          raise OutOfBound, "No more elements." if it.current.next_node.nil?

          value = it.current.next_node.value

          if it.current.next_node.__id__.eql?(@back.__id__)
            @back = it.current
            @back.send(:next_node=, nil)
          else
            it.current.send(:next_node=, it.current.next_node.next_node)
            it.current.next_node.send(:prev_node=, it.current)
          end

          @length = @size -= 1
          value
        end

        # Removes the element before given position, specified by the iterator.
        #
        # @param it [Node]
        # @raise ArgumentError, if the given iterator is invalid, or is not from type Node.
        # @raise EmptyContainerError, if the list is empty.
        # @raise OutOfBound, if there are no elements before the given iterator.
        # @return removed element
        def erase_before(it)
          raise ArgumentError, "Undefined method DoubleLinkedList#erase_before for #{it}:#{it.class}" unless it.is_a?(Iterator)
          raise ArgumentError, "Invalid iterator." unless !@front.nil? && it.current.id == @front.id
          raise EmptyContainerError, "The list is empty." if @front.nil?
          raise OutOfBound, "No more elements." if it.current.prev_node.nil?

          value = it.current.prev_node.value

          if it.current.prev_node.__id__.eql?(@front.__id__)
            @front = it.current
            @front.send(:prev_node=, nil)
          else
            it.current.send(:prev_node=, it.current.prev_node.prev_node)
            it.current.prev_node.send(:next_node=, it.current)
          end

          @length = @size -= 1
          value
        end

        # Checks if the list contains any elements.
        def empty?
          @front.nil?
        end

        # Compares contents of the list and a given as argument double linked list.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list.
        # @return [true, false] true, if contents are equal, false otherwise
        def ==(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#== for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          to_a == double_linked_list.to_a
        end

        # (see #==)
        def eql?(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#eql? for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          self == double_linked_list
        end

        # Compares contents of the list and a given as argument double linked list.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list.
        # @return [true, false] true, if contents are different, false otherwise
        def !=(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#!= for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          to_a != double_linked_list.to_a
        end

        # (see #!=)
        def diff?(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#diff? for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          self != double_linked_list
        end

        # Compares contents of the list and a given as argument double linked list.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list.
        # @return
        # 1 if content of the list is > than content of the given,
        # 0, if contents are euqal
        # -1, if content of the given list is > than content of the self object
        def <=>(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#== for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          to_a <=> double_linked_list.to_a
        end

        # Conerts content of the list to an array.
        def to_a
          convert
        end

        # Clears content of the list.
        def clear
          @front = @back = nil
          @length = @size = 0
          self
        end

        # Concatenates contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] a new object
        def +(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#+ for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)

          DoubleLinkedList.new(to_a + double_linked_list.to_a)
        end

        # (see #+)
        def concat(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#concat for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          self + double_linked_list
        end

        # (see #+)
        def merge(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#merge for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          self + double_linked_list
        end

        # Concatenates contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def concat!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#concat! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          from_obj(to_a + double_linked_list.to_a)
        end

        # Concatenates contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def merge!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#merge! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          from_obj(to_a + double_linked_list.to_a)
        end

        # Finds the union of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] a new object
        def |(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#| for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          DoubleLinkedList.new(to_a | double_linked_list.to_a)
        end

        # (see #|)
        def union(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#union for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          self | double_linked_list
        end

        # Finds the union of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def union!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#union! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          from_obj(to_a | double_linked_list.to_a)
        end

        # Finds the intersection of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] a new object
        def &(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#& for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          DoubleLinkedList.new(to_a & double_linked_list.to_a)
        end

        # (see #&)
        def intersect(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#intersect for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          self & double_linked_list
        end

        # Finds the intersection of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def intersect!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#intersect! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          from_obj(to_a & double_linked_list.to_a)
        end

        # Finds the difference of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] a new object
        def -(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#- for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          DoubleLinkedList.new(to_a - double_linked_list.to_a)
        end

        # (see #-)
        def difference(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#difference for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          self - double_linked_list
        end

        # Finds the difference of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def difference!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#difference! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          
          from_obj(to_a - double_linked_list.to_a)
        end

        # Finds the symmetric difference of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] a new object
        def ^(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#^ for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          self_a = to_a
          other = double_linked_list.to_a
          DoubleLinkedList.new((self_a | other) - (self_a & other))
        end

        # (see #^)
        def symmetric_difference(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#symmetric_difference for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          self ^ double_linked_list
        end

        # Finds the symmetric difference of contents of the list and a list given as argument.
        #
        # @param double_linked_list [DoubleLinkedList]
        # @raise ArgumentError, if given argument is not a double linked list
        # @return [DoubleLinkedList] self object
        def symmetric_difference!(double_linked_list)
          raise ArgumentError, "Undefined method DoubleLinkedList#symmetric_difference! for #{double_linked_list}:#{double_linked_list.class}" unless double_linked_list.is_a?(DoubleLinkedList)
          self_a = to_a
          other = double_linked_list.to_a
          from_obj((self_a | other) - (self_a & other))
        end

        # Filters elements of the list by given condition.
        #
        # @param &block
        # @return [DoubleLinkedList] a new linked list
        def select(&block)
          DoubleLinkedList.new(to_a.select { |e| block.call(e)})
        end

        # Filters elements of the list by given condition.
        #
        # @param &block
        # @return [DoubleLinkedList] self object
        def select!(&block)
          from_obj(to_a.select { |e| block.call(e)})
        end

        # (see #select)
        def filter(&block)
          select(&block)
        end

        # (see #select!)
        def filter!(&block)
          select!(&block)
        end

        # (see #select)
        def find_all(&block)
          select(&block)
        end

        # (see #select!)
        def find_all!(&block)
          select!(&block)
        end

        # Applies a function to each element of the list.
        # 
        # @param &block
        # @return [DoubleLinkedList] a new linked list
        def map(&block)
          DoubleLinkedList.new(to_a.map { |e| block.call(e)})
        end

        # Applies a function to each element of the list.
        # 
        # @param &block
        # @return [DoubleLinkedList] self object
        def map!(&block)
          from_obj(to_a.map { |e| block.call(e)})
        end

        # (see #map)
        def apply(&block)
          map(&block)
        end

        # (see #map!)
        def apply!(&block)
          map!(&block)
        end

        # Iterator for traversing the elements of the list.
        class Iterator
          attr_reader :current
          # Creates a new iterator.
          #
          # @param node [Node, DoubleLinkedList]
          def initialize(node)
             raise ArgumentError, "Invalid argument" unless node.is_a?(Node) || node.is_a?(DoubleLinkedList)

            @begin = @node.is_a?(Node) ? node : node.front
            @current = @node.is_a?(Node) ? node : node.front
            @prev_node = @node.is_a?(Node) ? node : node.front
            @next_node = @node.is_a?(Node) ? node : node.front
            
            @prev_node = @prev_node.prev_node
            @next_node = @next_node.next_node
          end

          # Resets the iterators to the initial state.
          def begin
            @current = @next_node = @begin
          end

          # Moves the iterator to the next node.
          def next
            raise OutOfBound, "No more elements." if @next_node.nil?
            @prev_node = @current
            @current = @next_node
            @next_node = @next_node.next_node
            self
          end

          # Moves the iterator to the previous element.
          def prev
            raise OutOfBound, "No more elements." if @prev_node.nil?
            @next_node = @current
            @current = @prev_node
            @prev_node = @prev_node.prev_node
            self
          end

          # Returns the value of the current position.
          def value
            @current.value
          end
        end

        private

        class Node
          attr_reader :id
          attr_accessor :value, :prev_node, :next_node

          def initialize(value, prev_node, next_node, id)
            @value = value
            @prev_node = prev_node
            @next_node = next_node
            @id = id
          end

          private :prev_node=, :next_node=
        end

        def from_obj(obj, copy = false)
          raise ArgumentError, "Undefined method #to_a for #{obj}:#{obj.class}" unless obj.respond_to?(:to_a)

          @front = @back = nil
          rand_id = Random.new.rand((2**(0.size * 8 -2) -1) / 2)
          obj.each do |v|
            if @front.nil?
              @front = @back = Node.new(copy ? v.dup : v, nil, nil, rand_id)
            else
              prev = @back
              @back = Node.new(copy ? v.dup : v, prev, nil, rand_id)
              prev.send(:next_node=, @back)
            end
          end

          @length = @size = obj.to_a.length
          self
        end

        def convert
          result = []
          it = @front
          until it.nil?
            result << it.value
            it = it.send(:next_node)
          end

          result
        end
      end
    end
  end
end