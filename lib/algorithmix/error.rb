module Algorithmix
  EmptyContainerError = Class.new(StandardError)
  OutOfBound          = Class.new(StandardError)
end