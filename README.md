# Algorithmix ![](https://img.shields.io/badge/min-0.0.0.7-green.svg) 

![](https://i.postimg.cc/LsXt1mcB/logo.png)

The gem contains various implementations data structures and algorithms. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'algorithmix'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install algorithmix

## Usage

For more information how to use the gem, check the [official documentation](https://github.com/monzita/algorithmix/wiki/).

## Contributing

Bug reports and pull requests are welcome on GitHub at [the official github page of the gem](https://github.com/monzita/algorithmix/). This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Algorithmix project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/monzita/algorithmix/blob/master/CODE_OF_CONDUCT.md).
