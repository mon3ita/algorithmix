RSpec.describe Algorithmix::DataStructure::Generic::Deque do

  before do
    @deque = Algorithmix::DataStructure::Generic::Deque.new
  end

  it "should insert new elements at the beginning" do
    @deque >> 1 >> 2 >> 3
    expect(@deque.to_a).to eq [3, 2, 1]
  end

  it "should insert new elements at the end" do
    @deque << 1 << 2 << 3
    expect(@deque.to_a).to eq [1, 2, 3]
  end

  it "should return element at given position" do
    @deque << 1 << 2 << 3
    expect(@deque[1]).to eq 2
  end

  it "should compare two deques" do
    @deque.assign([1, 2, 3])
    other = Algorithmix::DataStructure::Generic::Deque.new([4, 5, 6])
    expect(@deque == other).to be false
  end

  it "should filter elements of a deque" do
    @deque.assign([1, 2, 3, 4, 5])
    expect(@deque.select { |e| e.odd? }.to_a).to eq [1, 3, 5]
  end
end