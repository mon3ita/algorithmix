RSpec.describe Algorithmix::DataStructure::Generic::Stack do
  
  before do
    @stack = Algorithmix::DataStructure::Generic::Stack.new
  end

  it "should insert new elements into the stack" do
    @stack.push(1)
    expect(@stack.empty?).to be false
  end

  it "should compare two stacks" do
    other = Algorithmix::DataStructure::Generic::Stack.new([1, 2, 3])
    expect(@stack == other).to be false
  end

  it "should filter elements of a stack" do
    @stack.assign([1, 2, 3, 4, 5])
    expect(@stack.select { |e| e.even?}.to_a).to eq [2, 4]
  end

  it "should modify elements of a stack" do
    @stack.assign([1, 2, 3, 4, 5])
    expect(@stack.map { |e| e * 2 }.to_a).to eq [2, 4, 6, 8, 10]
  end
end
