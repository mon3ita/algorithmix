RSpec.describe Algorithmix::DataStructure::Generic::LinkedList do

  before do
    @ll = Algorithmix::DataStructure::Generic::LinkedList.new
  end

  it "should assign an object to the current list" do
    @ll.assign(1.upto(10))
    expect(@ll.to_a).to eq 10.downto(1).to_a
  end

  it "should remove front element of the current list" do
    @ll.assign(1.upto(10))
    expect(@ll.pop_front).to eq 10
  end

  it "should compare contents of two linked lists" do
    @ll.assign(1.upto(10))
    other = Algorithmix::DataStructure::Generic::LinkedList.new(11.upto(20))
    expect(@ll == other).to eq false
  end

  it "should concatenates contents of two linked lists" do
    @ll.assign(1.upto(10))
    other = Algorithmix::DataStructure::Generic::LinkedList.new(11.upto(20))
    expect((@ll + other).to_a).to eq 20.downto(1).to_a
  end

  it "should creates an iterator of a linked list" do
    @ll.assign(1.upto(20))
    it = Algorithmix::DataStructure::Generic::LinkedList::Iterator.new(@ll)
    it.next.next.next
    expect(it.current.value).to eq 17
  end

  it "should find the intersection of two linked lists" do
    @ll.assign(1.upto(10))
    other = Algorithmix::DataStructure::Generic::LinkedList.new(1.upto(20))
    expect((@ll & other).to_a).to eq 10.downto(1).to_a
  end

  it "should multiply each element of the list" do
    @ll.assign(1.upto(10))
    expect(@ll.map { |e| e * 2 }.to_a).to eq 1.upto(10).map { |e| e * 2 }.reverse
  end

  it "should return the value of the front element of a list" do
    @ll.assign(1.upto(10))
    expect{@ll.front.value}.to_not raise_error
    expect(@ll.front.value).to eq 10
  end

  it "should insert a new element after given position" do
    @ll.assign([2, 1])
    it = Algorithmix::DataStructure::Generic::LinkedList::Iterator.new(@ll)
    it.next
    @ll.insert_after(it, 3)
    expect(@ll.to_a).to eq 1.upto(3).to_a
  end

  it "should remove an element after given position" do
    @ll.assign([2, 1])
    it = Algorithmix::DataStructure::Generic::LinkedList::Iterator.new(@ll)
    @ll.erase_after(it)
    expect(@ll.to_a).to eq [1]
  end
end