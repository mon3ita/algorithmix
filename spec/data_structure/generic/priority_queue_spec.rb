RSpec.describe Algorithmix::DataStructure::Generic::PriorityQueue do

  before do
    @pq = Algorithmix::DataStructure::Generic::PriorityQueue.new
  end

  it "should assign correctly content of an object" do
    @pq.assign([1, 4, 2, 3, 7, 1, 11, 24, 1,323, 232321])
    expect(@pq.to_a).to eq  [232321, 323, 11, 24, 7, 1, 2, 3, 1, 4, 1]
  end

  it "should fix the heap property after removing an element of the queue" do
    @pq.assign(1.upto(10))
    @pq.pop
    expect(@pq.to_a).to eq  [9, 8, 7, 4, 5, 6, 3, 1, 2]
  end

  it "should concatenate two queues" do
    @pq.assign(1.upto(5))
    other = Algorithmix::DataStructure::Generic::PriorityQueue.new(5.upto(10))
    result = @pq + other
    expect(result.to_a).to eq  [10, 8, 9, 7, 6, 3, 5, 4, 1, 2, 5]
  end
end

