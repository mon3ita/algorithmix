RSpec.describe Algorithmix::DataStructure::Generic::DoubleLinkedList do

  before do
    @ll = Algorithmix::DataStructure::Generic::DoubleLinkedList.new
  end

  it "should insert new elements at the beginning of the list" do
    @ll >> 1 >> 2 >> 3 >> 4
    expect(@ll.to_a).to eq [4, 3, 2, 1]
  end

  it "should insert a new element at the beginning by given it" do
    @ll.assign(2.upto(10))
    it = Algorithmix::DataStructure::Generic::DoubleLinkedList::Iterator.new(@ll)
    @ll.insert_before(it, 1)
    expect(@ll.front.value).to eq 1
  end

  it "should insert a new element at the end by given it" do
    @ll.assign([1])
    it = Algorithmix::DataStructure::Generic::DoubleLinkedList::Iterator.new(@ll)
    @ll.insert_after(it, 2)
    expect(@ll.back.value).to eq 2
  end

  it "should remove an element from the end by given it" do
    @ll.assign([1, 2])
    it = Algorithmix::DataStructure::Generic::DoubleLinkedList::Iterator.new(@ll)
    val = @ll.erase_after(it)
    expect(val == 2 && @ll.back.value == 1).to be true
  end

  it "should remove an element from the beginning by given it" do
    @ll.assign([1, 2])
    it = Algorithmix::DataStructure::Generic::DoubleLinkedList::Iterator.new(@ll)
    it.next
    val = @ll.erase_before(it)
    expect(val == 1 && @ll.front.value == 2).to be true
  end
end