RSpec.describe Algorithmix::DataStructure::Generic::Queue do
  
  before do
    @queue = Algorithmix::DataStructure::Generic::Queue.new
  end

  it "should remove front elements of a queue" do
    @queue << 1 << 2 << 3
    expect(@queue.pop).to eq 1
  end

  it "should compare two queues" do
    @queue.assign([1, 2, 3, 4])
    other = Algorithmix::DataStructure::Generic::Queue.new([1, 3, 4, 5], copy: true)
    expect(@queue == other).to be false
  end

  it "should modify the content of a queue using map" do
    @queue << 1 << 2 << 3
    @queue.map! { |e| e * 2 }
    expect(@queue.to_a).to eq [2, 4, 6]
  end
end