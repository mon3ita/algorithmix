RSpec.describe Algorithmix::DataStructure::Heap::BinaryHeap do

  before do
    @heap = Algorithmix::DataStructure::Heap::BinaryHeap.new
  end

  it "should build a heap from given array" do
    @heap.assign(1.upto(7))
    expect(@heap.to_a).to eq [7, 5, 6, 4, 2, 1, 3]
  end

  it "should insert a new element in the right position" do
    @heap.assign(1.upto(5))
    @heap << 8 << 9 << 10
    expect(@heap.to_a).to eq [10, 9, 8, 4, 2, 3, 5, 1]
  end

  it "should increase a key in a heap" do
    @heap.assign(1.upto(5))
    @heap.increase_key(idx: 2, new_value: 10)
    expect(@heap.to_a).to eq [10, 4, 5, 1, 2]
  end

  it "should build a min heap" do
    min_heap = Algorithmix::DataStructure::Heap::BinaryHeap.new(5.downto(1), min: true)
    expect(min_heap.to_a).to eq [1, 2, 3, 5, 4]
  end

  it "should raise an error if increase_key is called on min heap" do
    min_heap = Algorithmix::DataStructure::Heap::BinaryHeap.new(1.upto(5), min: true)
    expect{min_heap.increase_key(idx: 1, new_value: 5)}.to raise_error(TypeError)
  end

  it "should raise an error if increase_key is called with unexisting element" do
    @heap.assign(1.upto(7))
    expect{@heap.increase_key(old_value: 9, new_value: 14)}.to raise_error(ArgumentError)
  end

  it "should increase_key if the given index is negative" do
    @heap.assign(1.upto(7))
    @heap.increase_key(idx: -1, new_value: 10)
    expect(@heap.to_a).to eq [10, 5, 7, 4, 2, 1, 6]
  end

  it "should decrease_key in a min heap" do
    min_heap = Algorithmix::DataStructure::Heap::BinaryHeap.new(10.downto(1), min: true)
    min_heap.decrease_key(idx: 5, new_value: -5)
    expect(min_heap.to_a).to eq [-5, 2, 1, 3, 6, 4, 8, 10, 7, 9]
  end

  it "should fix heap property after removing an element" do
    @heap.assign(1.upto(7))
    @heap.extract
    expect(@heap.to_a).to eq [6, 5, 3, 4, 2, 1]
  end

  it "should fix heap property in a min heap after removing min element" do
    min_heap = Algorithmix::DataStructure::Heap::BinaryHeap.new([1, 0, 2, 4, 5, 15, 7], min: true)
    min_heap.extract
    expect(min_heap.to_a).to eq [1, 4, 2, 7, 5, 15]
  end
end 